"""
Simulated Power Analyzer example.

This power analyzer simulation simulates charging and discharging a battery and measureing the voltage meanwhile.

The instrument plugin created by this example is accessible from a .NET API by referencing the built example directly.
From a .NET point of view, the assembly is called Python.PluginExample.dll and the instrument is named Python.PluginExample.PowerAnalyzer.
"""
import PythonTap
from PythonTap import *
from System import Double, Random, String  # Import types to reference for generic methods
from System.Diagnostics import Stopwatch
from System.Diagnostics import Debugger
import OpenTap
from OpenTap import DisplayAttribute

import inspect

# import AcSource
from .InstrumentAdapterP import InstrumentAdapter
from automation.AcSourceMod import AcSourceClass


@Attribute(DisplayAttribute, "AC source", "Interface to AC source", "HAT")
class AcSourceInst(InstrumentAdapter):
    def __init__(self):
        super().__init__()  # The base class initializer must be invoked.
        self.update_methods(AcSourceClass)

        prop = self.AddProperty("VisaAddress", "", String)
        prop.AddAttribute(DisplayAttribute, "Visa Address", "Address to connect to Instrument")

    def Open(self):
        super().Open()
        self.Debug(f"AC source Visa address: {self.VisaAddress}")
        self._intf = AcSourceClass(self.VisaAddress, reset=False)
        self._intf.clear()
        self.Info("AC source opened")

    def Close(self):
        self.Info("AC source closed")
        super().Close()