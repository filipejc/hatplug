import PythonTap
from PythonTap import *
from System import Double, Random, String, Int32, Boolean  # Import types to reference for generic methods
from System.Diagnostics import Stopwatch
from System.Diagnostics import Debugger
import OpenTap
from OpenTap import DisplayAttribute

import inspect


class InstrumentAdapter(Instrument):
    def __init__(self):
        super().__init__()  # The base class initializer must be invoked.

    def update_methods(self, InstrumentClass):
        instrument_methods = ["write", "check_errors", "query"]

        self.Info("Instrument Adapter")

        methods = inspect.getmembers(InstrumentClass, inspect.isroutine)
        for name, method in methods:
            self.Info(f"Method Name {name} in Instrument {name in instrument_methods}")
            if name.startswith("_") or name in instrument_methods:  # not parse private methods or instrument methods
                self.Debug(f"Method {name} ignored!")
                continue

            sig = inspect.signature(method)
            return_type = self.return_type(sig)
            self.Debug(f"Register method {name} with return type {return_type}")

            inst_method = self.RegisterMethod(name, return_type)

            for parameter in sig.parameters:
                if parameter == "self":
                    continue
                self.Info(f"Parameter {parameter}, Annotation {sig.parameters[parameter].annotation}")
                inst_method.AddArgument(parameter, self.get_type(sig.parameters[parameter].annotation))

    def get_type(self, annotation):
        plugin_class = None
        if annotation == int:
            plugin_class = Int32
        elif annotation == str:
            plugin_class = String
        elif annotation == float:
            plugin_class = Double
        elif annotation == bool:
            plugin_class = Boolean
        elif annotation == inspect.Signature.empty:
            plugin_class = None
        else:
            self.Warning(f"Type {annotation} not recognized")

        return plugin_class

    def return_type(self, sig):
        return_type = sig.return_annotation
        return self.get_type(return_type)

    # FIXME: this is used to call directly methods on the instruments,
    # best solution seems to have this as subclass or wrapper
    def __getattr__(self, method_name):
        def method(*args, **kwargs):
            self.Debug("Handling unknown method: '{}'".format(method_name))
            if kwargs:
                self.Debug("It had the following key word arguments: " + str(kwargs))
            if args:
                self.Debug("It had the following positional arguments: " + str(args))
            try:
                return getattr(self._intf, method_name)(*args, **kwargs)
            except Exception as e:
                exc_type, exc_value, exc_traceback = sys.exc_info()
                execption_text = traceback.format_exception(exc_type, exc_value, exc_traceback, limit=None, chain=True)

                self.Warning(f"Problem {method_name}, exception: {execption_text}")
                return None

        return method
