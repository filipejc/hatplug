"""
 A basic example of how to define a DUT driver.
"""
from PythonTap import *
from System import Double, Random, String #Import types to reference for generic methods
import OpenTap

from automation.Zigbee_interface import SecoInterfaceTelnet as SecoInterface
from automation.P7CM import P7CM_FCT

@Attribute(OpenTap.DisplayAttribute, "P7CM DUT", "P7CM DUT driver.", "Opentap Tester")
class P7CM_DUT(Dut):
    def __init__(self):
        super(P7CM_DUT, self).__init__() # The base class initializer must be invoked.
        self.AddProperty("Firmware", "1.0.2", None).AddAttribute(OpenTap.DisplayAttribute, "Firmware Version", "The firmware version of the DUT.", "Common")
        self.AddProperty("EUI", "00:13:A2:00:41:BC:00:07", String)
        self.AddProperty("SecoIP", "192.168.1.74", String)
        self.AddProperty("DaliUartIP", "192.168.1.70", String).AddAttribute(
            OpenTap.DisplayAttribute, "Dali Uart interface", "IP of Dali Uart interface", "Configurations")

        self.Name = "P7CM_DUT"

    def Open(self):
        """Called by TAP when the test plan starts."""
        self.Info(self.Name + " Opened")
        self._intf = SecoInterface(self.SecoIP)
        self._intf.set_node(self.EUI)
        # version = self._intf.execute_cli("version")
        # self.Info(f"P7CM version {version}")

        self._fct_intf = P7CM_FCT(self.DaliUartIP)

    def execute_cli(self, command):
        response = self._intf.execute_cli(command)
        self.Info(f"Command: '{command}', response: '{response}'")
        return response



    def Close(self):
        """Called by TAP when the test plan ends."""
        self.Debug(self.Name + " Closing")
        del self._intf
        self.Debug(self.Name + " Closed")