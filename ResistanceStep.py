"""
Sampling parent step for the power analyzer example.
Note that the step defined here in is supposed to be the base class for other test steps.
"""
import time
import sys
import math
import PythonTap
from PythonTap import *

import System
from System import Double, Int32
# Import types to reference for generic methods
from System.ComponentModel import BrowsableAttribute

from OpenTap import DisplayAttribute, UnitAttribute

from .PowermeterInstP import PowermeterInst
from .MultimeterInstP import MultimeterInst
from .AcSourceInstP import AcSourceInst
from .P7CM_DUT import P7CM_DUT
from .LucoTesterSettings import LucoTesterSettings

"""
A basic example of how to define settings.
This will show up in settings and cause a XML file to be generated in the Settings folder.
"""


@Attribute(DisplayAttribute, "Multimeter check", "Check connection with Multimeter", "HAT")
class MultimeterCheckStep(TestStep):
    def __init__(self):
        super().__init__()  # The base class initializer must be invoked.

        prop = self.AddProperty("Multimeter", None, MultimeterInst)
        prop.AddAttribute(DisplayAttribute, "Multimeter", "", "Resources", -100)

    def Run(self):
        self.Info("Multimeter OK")
        self.UpgradeVerdict(OpenTap.Verdict.Pass)


@Attribute(DisplayAttribute, "Resistance Test", "Check Resistance in Luco interface", "HAT")
class ResistanceTestStep(TestStep):
    def __init__(self):
        super().__init__()  # The base class initializer must be invoked.

        prop = self.AddProperty("Multimeter", None, MultimeterInst)
        prop.AddAttribute(DisplayAttribute, "Multimeter", "", "Resources")

        self._resistances_to_test = [
            "LineInToNeutral",
            "LineOutToNeutral",
            "LineInToLineOut",
            "DaliToGnd",
            "PsuToGnd",
            "LsiToGnd"
        ]
        for resistance in self._resistances_to_test:
            # Line In to Neutral
            self._create_resistance_attribute(resistance)

    def _create_resistance_attribute(self, name):
        # Line In to Neutral
        prop = self.AddProperty(f"{name}MaxResistance", 0, Double)
        prop.AddAttribute(DisplayAttribute, f"{name} Maximum resistance", "Maximum resistance",
                          "Measurements")
        prop.AddAttribute(OpenTap.UnitAttribute, "ohm")  # Property attributes can be added like this.

        prop = self.AddProperty(f"{name}MinResistance", 0, Double)
        prop.AddAttribute(DisplayAttribute, f"{name} Minimum resistance", "Minimum resistance",
                          "Measurements")
        prop.AddAttribute(OpenTap.UnitAttribute, "ohm")  # Property attributes can be added like this.

    def _check_resistance(self, name):
        # Line In to Line Out
        resistance = self.Multimeter.get_resistance_by_name(name)
        self.Info(f"{name} Resistance: {resistance}ohm")
        self.PublishResult("Resistance", ["Parameter", "Min", "Value", "Max", "Units"],
                           [f"{name} Resistance", getattr(self, f"{name}MinResistance"), resistance,
                            getattr(self, f"{name}MaxResistance"), "ohm"])

        if resistance < getattr(self, f"{name}MinResistance") or resistance > getattr(self, f"{name}MaxResistance"):
            return False
        else:
            return True

    def Run(self):
        veridict_pass = True

        # if one fail, all will fail.
        for name in self._resistances_to_test:
            veridict_pass &= self._check_resistance(name)

            if veridict_pass:  # only for pass
                self.UpgradeVerdict(OpenTap.Verdict.Pass)
            else:
                self.UpgradeVerdict(OpenTap.Verdict.Fail)


@Attribute(DisplayAttribute, "Voltage Test", "Check voltage in Luco interface", "HAT")
class VoltageTestStep(TestStep):
    def __init__(self):
        super().__init__()  # The base class initializer must be invoked.

        prop = self.AddProperty("Multimeter", None, MultimeterInst)
        prop.AddAttribute(DisplayAttribute, "Multimeter", "", "Resources")

        prop = self.AddProperty("AcSource", None, AcSourceInst)
        prop.AddAttribute(DisplayAttribute, "AcSource", "", "Resources")

        self._voltages_to_test = [
            {
                "name": "LineInToNeutral",
                "type": "AC",
            },
            {
                "name": "LineOutToNeutral",
                "type": "AC",
            },
            {
                "name": "LineInToLineOut",
                "type": "AC",
            },
            {
                "name": "DaliToGnd",
                "type": "DC",
            },
            {
                "name": "PsuToGnd",
                "type": "DC",
            },
            {
                "name": "LsiToGnd",
                "type": "DC",
            }
        ]
        for voltage in self._voltages_to_test:
            # Line In to Neutral
            self._create_voltage_attribute(voltage["name"])

    def _create_voltage_attribute(self, name):
        prop = self.AddProperty(f"{name}MaxVoltage", 0, Double)
        prop.AddAttribute(DisplayAttribute, f"{name} Maximum voltage", "Maximum voltage",
                          "Measurements")
        prop.AddAttribute(OpenTap.UnitAttribute, "V")  # Property attributes can be added like this.

        prop = self.AddProperty(f"{name}MinVoltage", 0, Double)
        prop.AddAttribute(DisplayAttribute, f"{name} Minimum voltage", "Minimum voltage",
                          "Measurements")
        prop.AddAttribute(OpenTap.UnitAttribute, "V")  # Property attributes can be added like this.

    def _check_voltage(self, name, meas_type):
        if meas_type == "DC":
            voltage = self.Multimeter.get_voltage_dc_by_name(name)
        elif meas_type == "AC":
            voltage = self.Multimeter.get_voltage_ac_by_name(name)
        else:
            raise ValueError(f"Meas type {meas_type} unknown")

        self.Info(f"{name} Voltage: {voltage} V")
        self.PublishResult("Voltage", ["Parameter", "Min", "Value", "Max", "Units"],
                           [f"{name} Voltage", getattr(self, f"{name}MinVoltage"), voltage,
                            getattr(self, f"{name}MaxVoltage"), "V"])

        if voltage < getattr(self, f"{name}MinVoltage") or voltage > getattr(self, f"{name}MaxVoltage"):
            return False
        else:
            return True

    def Run(self):
        verdict_pass = True

        self.Debug("AC source needs to be on")

        self.AcSource.set_output_state(True)
        self.Info("Wait to voltage to start")
        time.sleep(10)

        # if one fail, all will fail.
        for config in self._voltages_to_test:
            verdict_pass &= self._check_voltage(config["name"], config["type"])

        # stop power supply at the end
        self.AcSource.set_output_state(False)
        self.Info("Stop AC source")

        if verdict_pass:  # only for pass
            self.UpgradeVerdict(OpenTap.Verdict.Pass)
        else:
            self.UpgradeVerdict(OpenTap.Verdict.Fail)


@Attribute(DisplayAttribute, "AC source check", "Check connection with AC source", "HAT")
class ACSourceCheckStep(TestStep):
    def __init__(self):
        super().__init__()  # The base class initializer must be invoked.

        prop = self.AddProperty("ACsource", None, AcSourceInst)
        # prop = self.AddProperty("Multimeter", None, MultimeterInst)
        prop.AddAttribute(DisplayAttribute, "AC source", "", "Resources", -100)

    def Run(self):
        self.Info("AC source OK")
        self.UpgradeVerdict(OpenTap.Verdict.Pass)


@Attribute(DisplayAttribute, "Check automated inspect method test", "Check if automated test work", "HAT")
class ACSourceCheckReadingStep(TestStep):
    def __init__(self):
        super().__init__()  # The base class initializer must be invoked.

        prop = self.AddProperty("ACsource", None, AcSourceInst)
        # prop = self.AddProperty("Multimeter", None, MultimeterInst)
        prop.AddAttribute(DisplayAttribute, "AC source", "", "Resources", -100)

    def Run(self):
        self.Info("AC source OK")
        ac_voltage = self.ACsource.get_voltage_rms()
        self.Info(f"Current voltage: {ac_voltage}")
        self.UpgradeVerdict(OpenTap.Verdict.Pass)


@Attribute(DisplayAttribute, "Powermeter check", "Check connection with Powermeter", "HAT")
class PowermeterCheckStep(TestStep):
    def __init__(self):
        super().__init__()  # The base class initializer must be invoked.

        prop = self.AddProperty("Powermeter", None, PowermeterInst)
        # prop = self.AddProperty("Multimeter", None, MultimeterInst)
        prop.AddAttribute(DisplayAttribute, "Power meter", "", "Resources", -100)

    def Run(self):
        self.Info("Powermeter source OK")
        ac_voltage = self.Powermeter.get_voltage_rms()
        self.Info(f"Current voltage: {ac_voltage}")
        self.UpgradeVerdict(OpenTap.Verdict.Pass)


@Attribute(DisplayAttribute, "Relay check", "Check relay", "HAT")
class RelayTest(TestStep):
    def __init__(self):
        super().__init__()  # The base class initializer must be invoked.

        prop = self.AddProperty("P7CMDut", None, P7CM_DUT)
        # prop = self.AddProperty("Multimeter", None, MultimeterInst)
        prop.AddAttribute(DisplayAttribute, "P7CM DUT", "", "Resources", -100)

    def Run(self):
        self.Info("P7CM source OK")
        self.P7CMDut._fct_intf.execute_cmd(0x10, [0x03])
        self.UpgradeVerdict(OpenTap.Verdict.Pass)


@Attribute(DisplayAttribute, "ResistanceStep", "An example of a Resistance for a TestStep", "HAT")
@Attribute(AllowAnyChildAttribute)
class ResistanceStep(TestStep):
    def __init__(self):
        super(ResistanceStep, self).__init__()  # The base class initializer must be invoked.

        self._sampleNo = 0

        prop = self.AddProperty("Max_resistance", 20000, Double)
        prop.AddAttribute(UnitAttribute, "ohm")
        prop.AddAttribute(DisplayAttribute, "Max resistance", "Maximum resistance", "Measurements", -50)

        prop = self.AddProperty("Multimeter", None, MultimeterInst)
        prop.AddAttribute(DisplayAttribute, "Multimeter", "", "Resources", -100)

        self.AddProperty("Dut", None, P7CM_DUT).AddAttribute(OpenTap.DisplayAttribute, "P7CM_DUT",
                                                             "The P7CM DUT to use in the step.", "Resources")

    def Run(self):
        resistance = self.Multimeter.MeasureResistance()
        self.Info(f"Resistance {resistance}")
        self.PublishResult("Resistance", ["Resistance"], [resistance])

        if resistance < self.Max_resistance:
            self.UpgradeVerdict(OpenTap.Verdict.Pass)
        else:
            self.UpgradeVerdict(OpenTap.Verdict.Fail)

        response = self.Dut.execute_cli("adc")
        self.Info(f"ADC {response}")
