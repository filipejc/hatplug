# HAT #
**H**yperion **A**utomation **T**est

Python plugin for Opentap Test framework.

## Info ##
More information on Opentap:
* https://www.opentap.io/

This repository implement a Python plugin to support Luco Test automation under open tap.
* https://opentap.gitlab.io/Plugins/python/


### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

#### Install open tap ####
Go to opentap and follow the guide: <br>
https://www.opentap.io/download.html

#### Install Develop's System CE  ####
Locate install location of your open tap, normally: <br>
<code>C:\Program Files\OpenTAP\tap.exe</code>

Install Development package:<br>
<code>cd C:\Program Files\OpenTAP\tap.exe <br>
 tap package install "Developer's System CE"</code>

This will install the GUI editor and remaining packages can be installed from the GUI.

#### Editor ####
This is the editor GUI:
![Editor](Documentation/editor.png "Editor")

#### Install Python plugin SDK ####
Go to **Tools** Menu and open **Package Manager**.

![Package Manager](Documentation/Package_manager.png "Package Manager Menu")

Choose install in the Python plugin:

![Python plug in](Documentation/Python_plugin.png "Python plugin")

#### Set python path ####
Set python path with command:
<code>tap.exe python set-path "<folder of python>"</code>

**Example:**

<code>tap python set-path "C:\Users\fcoelho\AppData\Local\Continuum\anaconda3"<br>
Set Python path to 'C:\Users\fcoelho\AppData\Local\Continuum\anaconda3'</code>

#### Set your plugin path ####
Set the path where the opentap can find HAT plugin.

The following command can be used:

<code>tap python search-path --add "<folder of plugin>"</code>

**Example:**

<code> tap python search-path --add "C:\Users\fcoelho\OneDrive - Schréder SA\Documents\Schreder\Automation\HAT"<br>
Added' C:\Users\fcoelho\OneDrive - Schréder SA\Documents\Schreder\Automation\HAT' to the additional search path list</code>

It can be confirmed with following command:

<code>tap python search-path --get</code>


## Important notes ##
* Don't use spaces if not needed. Especially in attributes, it can cause problems.
* Objects are instanciate as they are created. So to see messages, you need to add and remove the instruments for example.
* Carefull with adding methods with not annotate arguments (empty class)
* objects seem to be created to compile plugin
* logging is not working on modules
* @Attribute(AllowAnyChildAttribute) can be used to allow for child steps
* Settings instance (GetCurrent) cannot be executed inside the \_\_init\_\_ for steps
* **Todo:** Settings work for basic example but not for our plugin 
