import PythonTap
from PythonTap import *
from System import Double, Random, String  # Import types to reference for generic methods
from System.Diagnostics import Stopwatch
from System.Diagnostics import Debugger
import OpenTap
from OpenTap import DisplayAttribute

import inspect

# import AcSource
from .InstrumentAdapterP import InstrumentAdapter
from automation.PM1000Mod import PM1000Class


@Attribute(DisplayAttribute, "Power meter", "Interface to Power meter", "HAT")
class PowermeterInst(InstrumentAdapter):
    def __init__(self):
        super().__init__()  # The base class initializer must be invoked.
        self.update_methods(PM1000Class)

        prop = self.AddProperty("VisaAddress", "", String)
        prop.AddAttribute(DisplayAttribute, "Visa Address", "Address to connect to Instrument")

    def Open(self):
        super().Open()
        self.Debug(f"Power meter source Visa address: {self.VisaAddress}")
        self._intf = PM1000Class(self.VisaAddress, reset=False)
        self.Info("Powermeter opened")

    def Close(self):
        self.Info("Powermeter closed")
        super().Close()
