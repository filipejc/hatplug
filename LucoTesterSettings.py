"""
A basic example of how to define settings.
This will show up in settings and cause a XML file to be generated in the Settings folder.
"""
from PythonTap import *
import OpenTap
from System import Int32

@Attribute(OpenTap.DisplayAttribute, "Example Settings", "Luco Tester Settings.")
class LucoTesterSettings(ComponentSettings):
    def __init__(self):
        super(LucoTesterSettings, self).__init__() # The base class initializer must be invoked.
        prop = self.AddProperty("ChannelLineInToNeutral", 600, Int32);
        prop.AddAttribute(OpenTap.DisplayAttribute, "Channel Line In To Neutral")